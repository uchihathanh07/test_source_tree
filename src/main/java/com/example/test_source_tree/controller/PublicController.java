package com.example.test_source_tree.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicController {
    @GetMapping("test")
    public String testString(){
        return "Thành công rùi";
    }

    @GetMapping("P620")
    public String callP620(){
        return "Trả về P620";
    }

    @GetMapping("p840")
    public String callP840(){
        return "Trả về P840";
    }
}
